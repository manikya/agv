package lk.kadiya.test.apitestautomation.motioncommand;

import lk.kadiya.test.apitestautomation.command.CommandKey;
import lk.kadiya.test.apitestautomation.operation.ControlParameters;

public class BreakCommand implements MotionCommand {
    @Override
    public String commandKey() {
        return CommandKey.Break.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        return null;
    }
}