package lk.kadiya.test.apitestautomation.motioncommand;

import lk.kadiya.test.apitestautomation.command.CommandKey;
import lk.kadiya.test.apitestautomation.operation.ControlParameters;
import lk.kadiya.test.apitestautomation.operation.MotionOperation;
import lk.kadiya.test.apitestautomation.operation.MotionOperationBuilder;
import lk.kadiya.test.apitestautomation.service.OUTPIN;
import lk.kadiya.test.apitestautomation.service.PWMPIN;
import lk.kadiya.test.apitestautomation.util.DelayUtil;

import java.util.ArrayList;
import java.util.List;

public class MoveForwardCommand implements MotionCommand {
    @Override
    public String commandKey() {
        return CommandKey.MoveForward.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        //move forward at full speed passed
        MotionOperationBuilder builder = new MotionOperationBuilder(parameters).setPercentage(100);

        List<MotionOperation> operations = new ArrayList<>(2);
        operations.add(builder.setPin(PWMPIN.RIGHT_FRONT).build());
        operations.add(builder.setPin(PWMPIN.LEFT_FRONT).build());

        setSpeed(operations);

        //move time
        DelayUtil.mediumDelay();
        //reset speed
        resetSpeed(operations);
        return "Move forward success ";
    }
}