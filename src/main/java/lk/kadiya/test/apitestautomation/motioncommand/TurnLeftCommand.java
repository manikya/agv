package lk.kadiya.test.apitestautomation.motioncommand;

import lk.kadiya.test.apitestautomation.command.CommandKey;
import lk.kadiya.test.apitestautomation.operation.ControlParameters;
import lk.kadiya.test.apitestautomation.operation.MotionOperation;
import lk.kadiya.test.apitestautomation.operation.MotionOperationBuilder;
import lk.kadiya.test.apitestautomation.service.OUTPIN;
import lk.kadiya.test.apitestautomation.service.PWMPIN;
import lk.kadiya.test.apitestautomation.util.DelayUtil;

import java.util.ArrayList;
import java.util.List;

public class TurnLeftCommand implements MotionCommand {
    @Override
    public String commandKey() {
        return CommandKey.TurnLeft.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        //move forward at 70% speed passed
        MotionOperationBuilder builder = new MotionOperationBuilder(parameters).setPercentage(70);

        List<MotionOperation> operations = new ArrayList<>(2);
        //Move right front forward
        operations.add(builder.setPin(PWMPIN.RIGHT_FRONT).build());
        //Move left front backward
        operations.add(builder.setPin(PWMPIN.LEFT_BACK).build());

        setSpeed(operations);

        //move time
        DelayUtil.smallDelay();
        //reset speed
        resetSpeed(operations);
        return "Move left success ";
    }
}
