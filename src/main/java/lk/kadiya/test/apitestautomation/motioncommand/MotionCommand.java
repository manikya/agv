package lk.kadiya.test.apitestautomation.motioncommand;

import lk.kadiya.test.apitestautomation.command.Command;
import lk.kadiya.test.apitestautomation.operation.MotionOperation;
import lk.kadiya.test.apitestautomation.service.PWMPIN;
import lk.kadiya.test.apitestautomation.service.PinHandler;

import java.util.List;

public interface MotionCommand extends Command {

    default void setSpeed(List<MotionOperation> operations) {
        for (MotionOperation op : operations) {
            int speed = calcPeed(op);
            PinHandler.getPwmPin(op.getPin()).setPwm(speed);
            log( "Pin "+op.getPin() +" speed " +speed);
        }
    }

    default int calcPeed(MotionOperation operation) {
        int speed = getCalibratedSpeed(operation.getPin(), operation.getGeneralSpeed());
        return  (speed * operation.getPercentage()) / 100;
    }

    default int getCalibratedSpeed(PWMPIN pwmpin, int generalSpeed) {
        return generalSpeed;
    }

    default void resetSpeed(List<MotionOperation> operations) {
        for (MotionOperation op : operations) {
            PinHandler.getPwmPin(op.getPin()).setPwm(0);
            log( "Pin "+op.getPin() +" reset speed to speed 0");
        }
    }
}
