package lk.kadiya.test.apitestautomation.operation;

import lk.kadiya.test.apitestautomation.service.OUTPIN;
import lk.kadiya.test.apitestautomation.service.PWMPIN;

public class MotionOperationBuilder {
    private int generalSpeed;
    private boolean isForward = true;
    private int percentage = 100;
    private PWMPIN pin;
    private OUTPIN controlPin;

    public MotionOperationBuilder(ControlParameters parameters) {
        this.generalSpeed = parameters.getGeneralSpeed();
    }

    public MotionOperationBuilder isForward(boolean isForward) {
        this.isForward = isForward;
        return this;
    }

    public MotionOperationBuilder setPercentage(int percentage) {
        this.percentage = percentage;
        return this;
    }

    public MotionOperationBuilder setPin(PWMPIN pin) {
        this.pin = pin;
        return this;
    }

    public MotionOperationBuilder setControlPin(OUTPIN controlPin) {
        this.controlPin = controlPin;
        return this;
    }

    public MotionOperation build() {
        return new MotionOperation(generalSpeed, isForward, percentage, pin, controlPin);
    }
}