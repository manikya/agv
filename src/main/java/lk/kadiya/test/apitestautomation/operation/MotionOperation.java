package lk.kadiya.test.apitestautomation.operation;

import lk.kadiya.test.apitestautomation.service.OUTPIN;
import lk.kadiya.test.apitestautomation.service.PWMPIN;

public class MotionOperation {
    private int generalSpeed;
    private boolean isForward = true;
    private int percentage = 100;
    private PWMPIN pin;
    private OUTPIN controlPin;

    public MotionOperation() {
    }

    public MotionOperation(int generalSpeed, boolean isForward, int percentage, PWMPIN pin, OUTPIN controlPin) {
        this.generalSpeed = generalSpeed;
        this.isForward = isForward;
        this.percentage = percentage;
        this.pin = pin;
        this.controlPin = controlPin;
    }

    public boolean isForward() {
        return isForward;
    }

    public void setForward(boolean forward) {
        this.isForward = forward;
    }

    public int getGeneralSpeed() {
        return generalSpeed;
    }

    public void setGeneralSpeed(int generalSpeed) {
        this.generalSpeed = generalSpeed;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }

    public PWMPIN getPin() {
        return pin;
    }

    public void setPin(PWMPIN pin) {
        this.pin = pin;
    }

//    public int getDirectionInt() {
//        return isForward ? 1 : -1;
//    }

    public OUTPIN getControlPin() {
        return controlPin;
    }

    public void setControlPin(OUTPIN controlPin) {
        this.controlPin = controlPin;
    }
}
