package lk.kadiya.test.apitestautomation.operation;

public class ControlParameters {
    private int generalSpeed;

    public ControlParameters(int generalSpeed) {
        this.generalSpeed = generalSpeed;
    }

    public int getGeneralSpeed() {
        return generalSpeed;
    }

    public void setGeneralSpeed(int generalSpeed) {
        this.generalSpeed = generalSpeed;
    }
}
