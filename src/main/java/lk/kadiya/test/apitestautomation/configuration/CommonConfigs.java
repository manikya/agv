package lk.kadiya.test.apitestautomation.configuration;

import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CommonConfigs {
    private static final String filepath = CommonConfigs.class.getSimpleName();
    private static CommonConfigs instance;

    public static CommonConfigs getInstance() {
        if (instance == null) {
            instance = new CommonConfigs();
            try {
                instance = loadFromFile();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        return instance;
    }

    private CommonConfigs() {

    }

    private static CommonConfigs loadFromFile() throws FileNotFoundException {
        Gson gson = new Gson();
        return gson.fromJson(new FileReader(filepath), CommonConfigs.class);
    }

    private static void saveToFile() {
        Gson gson = new Gson();
        try {
            gson.toJson(instance, new FileWriter(filepath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public int defaultSpeed = 100;
}
