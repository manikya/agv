package lk.kadiya.test.apitestautomation;

import lk.kadiya.test.apitestautomation.domain.ChartData;
import lk.kadiya.test.apitestautomation.domain.SensorData;

import java.util.ArrayList;

public class Mapper {
    public static ChartData mapToChart(SensorData data) {
        ChartData chartData = new ChartData();
        if (data == null) {
            return chartData;
        }
        chartData.setTimestamp(data.getTimestamp());
        chartData.setTemp(data.getTemperature());
        chartData.setBmp(String.valueOf(data.getBPM()));
        chartData.setSpo2(String.valueOf(data.getSPO2()));
        ArrayList<Double> vals = new ArrayList<>();
        for (double val : data.getECG()) {
            vals.add(val);
        }
        chartData.setEcg(vals);

        return chartData;

    }
}
