package lk.kadiya.test.apitestautomation.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Arrays;

public class SensorData {
    @JsonProperty("Temperature")
    private double Temperature;
    @JsonProperty("SPO2")
    private int SPO2;
    @JsonProperty("ECG")
    private double[] ECG;
    @JsonProperty("Timestamp")
    private long Timestamp;
    @JsonProperty("BPM")
    private int BPM;

    public double getTemperature() {
        return Temperature;
    }

    public void setTemperature(double temperature) {
        Temperature = temperature;
    }

    public int getSPO2() {
        return SPO2;
    }

    public void setSPO2(int SPO2) {
        this.SPO2 = SPO2;
    }

    public double[] getECG() {
        return ECG;
    }

    public void setECG(double[] ECG) {
        this.ECG = ECG;
    }

    public long getTimestamp() {
        return Timestamp;
    }

    public void setTimestamp(long timestamp) {
        Timestamp = timestamp;
    }

    public int getBPM() {
        return BPM;
    }

    public void setBPM(int BPM) {
        this.BPM = BPM;
    }

    @Override
    public String toString() {
        return "SensorData{" +
                "Temperature=" + Temperature +
                ", SPO2=" + SPO2 +
                ", ECG=" + Arrays.toString(ECG) +
                ", Timestamp='" + Timestamp + '\'' +
                ", BPM=" + BPM +
                '}';
    }
}
