package lk.kadiya.test.apitestautomation.domain;

import java.util.ArrayList;

public class ChartData {
    private long timestamp;
    private double temp;
    private String bmp;
    private String spo2;
    private ArrayList<Double> ecg;

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getTemp() {
        return temp;
    }

    public void setTemp(double temp) {
        this.temp = temp;
    }

    public String getBmp() {
        return bmp;
    }

    public void setBmp(String bmp) {
        this.bmp = bmp;
    }

    public String getSpo2() {
        return spo2;
    }

    public void setSpo2(String spo2) {
        this.spo2 = spo2;
    }

    public ArrayList<Double> getEcg() {
        return ecg;
    }

    public void setEcg(ArrayList<Double> ecg) {
        this.ecg = ecg;
    }

    @Override
    public String toString() {
        return "ChartData{" +
                "timestamp=" + timestamp +
                ", temp=" + temp +
                ", bmp='" + bmp + '\'' +
                ", spo2='" + spo2 + '\'' +
                ", ecg=" + ecg +
                '}';
    }
}
