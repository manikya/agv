package lk.kadiya.test.apitestautomation.controller;

import lk.kadiya.test.apitestautomation.Mapper;
import lk.kadiya.test.apitestautomation.domain.ChartData;
import lk.kadiya.test.apitestautomation.domain.SensorData;
import lk.kadiya.test.apitestautomation.util.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class DataController {

    public static SensorData data = null;

    Logger logger = LoggerFactory.getLogger(DataController.class);

    @CrossOrigin
    @PostMapping(path = "/sensor")
    public ResponseEntity<Response> updateValues(@RequestBody SensorData data) {
        logger.info(data.toString());
        DataController.data = data;
        return ResponseEntity.ok(new Response("Success"));
    }

    @CrossOrigin
    @GetMapping(path = "/sensor")
    public ResponseEntity<ChartData> getValues() {
        ChartData chartData = Mapper.mapToChart(DataController.data);
        logger.info(chartData.toString());
        DataController.data = null;
        return ResponseEntity.ok(chartData);
    }
}
