package lk.kadiya.test.apitestautomation.controller;

import lk.kadiya.test.apitestautomation.command.Command;
import lk.kadiya.test.apitestautomation.command.CommandExecutor;
import lk.kadiya.test.apitestautomation.command.CommandFactory;
import lk.kadiya.test.apitestautomation.command.CommandKey;
import lk.kadiya.test.apitestautomation.operation.ControlParameters;
import lk.kadiya.test.apitestautomation.util.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/command")
public class CommandController {

    Logger logger = LoggerFactory.getLogger(CommandController.class);

    @CrossOrigin
    @GetMapping(path = "/run")
    public ResponseEntity<Response> runCommand(@RequestParam(value = "command") CommandKey commandKey, @RequestParam(value="speed", required = false ) int generalSpeed) {
        logger.info("Command Received " + commandKey.toString());
        Command command = CommandFactory.getCommand(commandKey);
        ControlParameters  controlParameters = new ControlParameters(generalSpeed);
        String result = CommandExecutor.runCommand(command,controlParameters);
        return ResponseEntity.ok(new Response(result));
    }

}
