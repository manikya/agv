package lk.kadiya.test.apitestautomation.controller;

import lk.kadiya.test.apitestautomation.util.Delay;
import lk.kadiya.test.apitestautomation.util.DelayUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/delay")
public class DelayController {

    @CrossOrigin
    @GetMapping(path = "/set")
    public ResponseEntity<String> updateDelay(@RequestParam(value = "type") Delay type, @RequestParam long value) {
        System.out.println("Value Received " + type);
        switch (type) {
            case Small:
                DelayUtil.smallDelay = value;
                break;
            case Medium:
                DelayUtil.mediumDelay = value;
                break;
            case Large:
                DelayUtil.largeDelay = value;
                break;
        }
        return ResponseEntity.ok("value set success");
    }

    @CrossOrigin
    @GetMapping(path = "/get")
    public ResponseEntity<String> getDelay() {
        return ResponseEntity.ok("Small = " + DelayUtil.smallDelay + " Medium = " + DelayUtil.mediumDelay + " Large = " + DelayUtil.largeDelay);
    }


}
