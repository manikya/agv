package lk.kadiya.test.apitestautomation.command;

import lk.kadiya.test.apitestautomation.operation.ControlParameters;

public class Button3Command implements Command {
    @Override
    public String commandKey() {
        return CommandKey.command3.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        return "Command 3 running";
    }
}
