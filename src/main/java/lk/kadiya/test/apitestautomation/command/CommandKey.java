package lk.kadiya.test.apitestautomation.command;

public enum CommandKey {
    BlinkLED,

    //Command buttons
    command1,
    command2,
    command3,
    command4,

    //Motion commands
    TurnLeft,
    TurnRight,
    MoveForward,
    MoveBackward,
    Break
}
