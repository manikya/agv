package lk.kadiya.test.apitestautomation.command;

import lk.kadiya.test.apitestautomation.motioncommand.MotionCommand;
import lk.kadiya.test.apitestautomation.operation.ControlParameters;

public class CommandExecutor {

    public static String runCommand(Command command, ControlParameters parameters) {
        try {
            if (command instanceof MotionCommand) {
                return runMotionCommand(command, parameters);
            }
            return runCommandAsynchronously(command, parameters);
        } catch (Exception e) {
            e.printStackTrace();
            return "Error in running command " + command.commandKey() + " . " + e.getMessage();
        }
    }

    private static String runMotionCommand(Command command, ControlParameters parameters) throws Exception {
        return runCommandSynchronously(command, parameters);
    }

    private static synchronized String runCommandSynchronously(Command command, ControlParameters parameters) throws Exception {
        command.log("Running Synchronously");
        return "Sync : " + command.run(parameters);
    }

    private static String runCommandAsynchronously(Command command, ControlParameters parameters) throws Exception {
        command.log("Running Asynchronously");
        return "Async : " + command.run(parameters);
    }
}
