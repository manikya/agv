package lk.kadiya.test.apitestautomation.command;

import lk.kadiya.test.apitestautomation.operation.ControlParameters;

public class Button2Command implements Command {
    @Override
    public String commandKey() {
        return CommandKey.command2.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        return "Command 2 running";
    }
}
