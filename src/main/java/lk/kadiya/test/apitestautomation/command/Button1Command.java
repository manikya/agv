package lk.kadiya.test.apitestautomation.command;

import lk.kadiya.test.apitestautomation.operation.ControlParameters;

public class Button1Command implements Command {
    @Override
    public String commandKey() {
        return CommandKey.command1.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        return "Command 1 running";
    }
}
