package lk.kadiya.test.apitestautomation.command;

import lk.kadiya.test.apitestautomation.operation.ControlParameters;

public class Button4Command implements Command {
    @Override
    public String commandKey() {
        return CommandKey.command4.toString();
    }

    @Override
    public String run(ControlParameters parameters) throws Exception {
        return "Command 4 running";
    }
}
